import Koa from 'koa';

const PORT = process.env.port || 3000;
const app = new Koa();
app.use(ctx => { ctx.body = 'Hello, Restream'; });
app.listen(PORT);
