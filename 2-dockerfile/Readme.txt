1) Собрать docker file с названием t2:latest

docker build . -t t2:latest

2) Посмотреть, что образ собран
docker images | grep t2

3) Запустить контейнер с собранным образом и вывести наружу порт 3000
docker run --rm --name hello -p 3000:3000 -ti t2:latest

curl localhost:3000
