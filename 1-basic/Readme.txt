Чтобы запустить контейнер, надо набрать

1) Запуск контейнера

docker run alpine ping alexbyk.com
  это запустить образ alpine:latest из докерхаба со стартовой командой ping

2) Более детальный запуск контейнера

docker run -ti --rm --name=test ubuntu:latest bash

Запустить команду баш, сделать, чтобы работал терминал в нем, и удалить при выходе (не будет статуса exited)

3) Инспекция контейнеров

Запущенные:
docker ps

все
docker ps -a

посмотреть информацию о контейнере

docker inspect test
docker inspect --format='{{.NetworkSettings.IPAddress}}' test

4) Режим демона и приатачиться
docker run -d -ti --rm --name=test ubuntu:latest bash
docker attach test
